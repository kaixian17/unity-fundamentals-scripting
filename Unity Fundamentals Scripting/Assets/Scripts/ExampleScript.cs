﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScript : MonoBehaviour
{
    public float speed = 0f;
    public float distance = 0f;
    public float time = 0f;
    public float maxSpeedLimit = 70f;
    public float minSpeedLimit = 40f;

    void Start()
    {
       
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SpeedCheck();
        }
    }

    void SpeedCheck()
    {
        speed = distance / time;

        if(speed>=maxSpeedLimit)
        {
            print("Te estás pasando el límite de velocidad");
        }

        else if(speed<=minSpeedLimit)
        {
            print("Estás yendo muy lento");
        }

        else if (speed == maxSpeedLimit || speed == minSpeedLimit)
        {
            print("Estás a punto de romper las reglas");
        }

        else
        {
            print("Estás dentro del límite de velocidad");
        }
      
    }
}
