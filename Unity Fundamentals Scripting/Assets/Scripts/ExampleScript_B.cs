﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScript_B : MonoBehaviour
{
    private int enemyDistance = 0;
    private int enemyCount = 10;
    private string[] enemies = new string[5];
    private int weaponId = 0;

    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //EnemySearch();
            //EnemyDestruction();
            //EnemyScan();
            //EnemyRoster();
            WeaponSearch();
        }
    }

    void EnemySearch()
    {
        for(int i=0;i<5;i++)
        {
            enemyDistance = Random.Range(1, 10);

            if(enemyDistance>7)
            {
                print("El enemigo está muy lejos");
            }

            if (enemyDistance >= 4 && enemyDistance <= 7) 
            {
                print("El enemigo está en rango medio");
            }

            if (enemyDistance <4)
            {
                print("El enemigo está muy cerca");
            }
        }
    }

    void EnemyDestruction()
    {
        while (enemyCount>0)
        {
            print("Hay un enemigo, vamos a destruirlo");
            enemyCount--;
        }


    }

    void EnemyScan()
    {
        bool isAlive = false;

        do
        {
            print("Escaneando enemigos...");

        }
        while (isAlive == true);
    }

    void EnemyRoster()
    {
        enemies[0] = "Orc";
        enemies[1] = "Dragon";
        enemies[2] = "Dwarf";
        enemies[3] = "Slime";
        enemies[4] = "Dark Knight";

        foreach(string enemy in enemies)
        {
            print(enemy);
        }
    }
    
    void WeaponSearch()
    {
        weaponId = Random.Range(1, 8);

        switch (weaponId)
        {
            case 1:
                print("Has encontrado un palo");
                break;
            case 2:
                print("Has encontrado una lanza");
                break;
            case 3:
                print("Has encontrado una espada");
                break;
            case 4:
                print("Has encontrado una guadaña");
                break;
            default:
                print("No has encontrado nada");
                break;
        }
    }
    
}
